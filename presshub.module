<?php

use Drupal\Core\Entity\EntityInterface;
use Drupal\presshub\PresshubHelper;
use Drupal\Core\Url;

/**
 * Implements hook_page_attachments().
 */
function presshub_page_attachments(array &$page) {
  $entity = \Drupal::routeMatch()->getParameter('node');
  if ($entity) {
    $entity_id = $entity->id();
    $presshub = new PresshubHelper();
    $amp_content = $presshub->getAmpVersion($entity_id);
    if (!empty($amp_content)) {
      // Add path to AMP version of articles
      $options = ['absolute' => TRUE];
      $url = Url::fromRoute('presshub.amp.node', ['entity_id' => $entity_id], $options);
      $amp_version = [
        '#tag' => 'link',
        '#attributes' => [
          'rel' => 'amphtml',
          'href' => $url->toString(),
        ],
      ];
      $page['#attached']['html_head'][] = [$amp_version, 'amp_version'];
    }
  }
}

/**
 * Implements hook_entity_insert().
 */
function presshub_entity_insert(EntityInterface $entity) {
  $presshub = new PresshubHelper();
  if ($template = $presshub->generateTemplate($entity)) {
    $presshub->publish($entity, $template);
  }
}

/**
 * Implements hook_entity_update().
 */
function presshub_entity_update(EntityInterface $entity) {
  $presshub = new PresshubHelper();
  if ($template = $presshub->generateTemplate($entity)) {
    if ($article = $presshub->isPublished($entity)) {
      $presshub->update($article, $entity, $template);
    }
    else {
      $presshub->publish($entity, $template);
    }
  }
}

/**
 * Implements hook_entity_delete().
 */
function presshub_entity_delete(EntityInterface $entity) {
  $presshub = new PresshubHelper();
  if ($article = $presshub->isPublished($entity)) {
    $presshub->delete($article, $entity);
  }
}
